# Python Syntax
# Hello World
print("Hello World")

full_name = "John Doe"
age = 23

print("My name is " + full_name)
# print("My age is " + age)
print("My age is " + str(age))
print(f"My age is {age}")
print(int(3.5))
print(int("9861"))