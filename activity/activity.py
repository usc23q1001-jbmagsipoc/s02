name = "John Benedict"
age = 23
occupation = "Intern"
movie = "Spider-Man: Into the Spider-Verse"
rating = 97.9

print(f"I am {name}, and I am {age} years old, I work as an {occupation}, and my rating for {movie} is {rating}%")

num1, num2, num3 = 5, 4, 3

print(num1 * num2)
print(num1 < num3)
print(num3 + num2)